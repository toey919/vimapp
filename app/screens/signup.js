import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Platform,
    Dimensions
} from 'react-native';

import { CheckboxField, Checkbox } from 'react-native-checkbox-field';

export default class signup extends React.Component{

  constructor(props) {
      super(props);

      this.state = {
          selected: false,
          fieldLabel: 'Mostrar contraseña.'
      };

      this.selectCheckbox = this.selectCheckbox.bind(this);
  }

  selectCheckbox() {
      this.setState({
          selected: !this.state.selected
      });
  }

  render(){
    return (
      <View style={styles.mainContainer}>
        <Image source={require('../img/register.png')} style={styles.backgroundImage} >
          <ScrollView style={styles.form}>
            <View style={styles.username}>
              <Image source={require('../img/usuario.png')} style={styles.usericon} />
              <TextInput
                style={styles.usertext}
                placeholder='Usuario'
              />
            </View>
            <View style={styles.password}>
              <Image source={require('../img/Mail.png')} style={styles.passicon} />
              <TextInput
                style={styles.usertext}
                placeholder='Correo'
              />
            </View>
            <View style={styles.password}>
              <Image source={require('../img/candado.png')} style={styles.passicon} />
              <TextInput
                style={styles.usertext}
                placeholder='Contraseña'
                password={true}
              />
            </View>
            <CheckboxField
                label={this.state.fieldLabel}
                onSelect={this.selectCheckbox}
                disabled={this.props.disabled}
                disabledColor='rgb(236,236,236)'
                selected={this.state.selected}
                defaultColor={'rgba(0,0,0,0)'}
                selectedColor="#4a2d8e"
                containerStyle={styles.containerStyle}
                labelStyle={styles.labelStyle}
                checkboxStyle={styles.checkboxStyle}
                labelSide="right">
            </CheckboxField>
            <View style={styles.loginBtn}>
              <TouchableOpacity style={styles.loginbutton} onPress={this.props.onPress}>
                <Text style={styles.loginBtntext}>Regístrate</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.facebook}>
              <TouchableOpacity style={styles.facebutton} onPress={this.props.onPress}>
                <Image source={require('../img/Fill 3.png')} style={styles.faceicon} />
                <Text style={styles.facetext}>Entrar con Facebook</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <View style={styles.bottom}>
            <Text style={styles.bottomtext}>Accediendo aceptas los Términos y Condiciones y las Políticas de privacidad.</Text>
          </View>
        </Image>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  backgroundImage: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    width: null,
    height: null,
    resizeMode: 'stretch'
  },
  mainContainer: {
    flex: 1
  },
  form: {
    flex:1,
    flexDirection:'column',
    padding:60,
    width:Dimensions.get('window').width,
    height:Dimensions.get('window').height,
    marginTop:120
  },
  username: {
    flexDirection: 'row',
    width: Dimensions.get('window').width/2 * 1.33,
    height: 37,
    alignSelf: 'center',
    backgroundColor: 'white',
    borderRadius: 4
  },
  usericon: {
    width: 25,
    height: 25,
    marginLeft: 10,
    marginTop: 6,
    resizeMode: 'center'
  },
  usertext: {
    width: Dimensions.get('window').width/2 * 1.33 - 40,
    height: 37,
    marginLeft: 7
  },
  password: {
    flexDirection: 'row',
    width: Dimensions.get('window').width/2 * 1.33,
    height: 37,
    marginTop: 12,
    alignSelf: 'center',
    backgroundColor: 'white',
    borderRadius: 4
  },
  passicon: {
    width: 25,
    height: 27,
    marginLeft: 10,
    marginTop: 5,
    resizeMode: 'center'
  },
  loginBtn: {
    width: Dimensions.get('window').width/2 * 1.33,
    height: 37,
    marginTop: 6,
    alignSelf: 'center',
    borderRadius: 4
  },
  loginBtntext: {
    width: Dimensions.get('window').width/2 * 1.33 - 100,
    height: 37,
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
    paddingTop: 9,
    alignSelf: 'center'
  },
  loginbutton: {
    width: Dimensions.get('window').width/2 * 1.33,
    justifyContent: 'center',
    borderRadius: 4,
    backgroundColor: '#4a2d8e',
  },
  forgotBtn: {
    flexDirection: 'row',
    width: Dimensions.get('window').width/2 * 1.33,
    height: 20,
    marginTop: 12,
    alignSelf: 'center',
  },
  forgot: {
    width: Dimensions.get('window').width/2 * 1.33,
    height: 20,
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 14,
    color: 'white',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  facebook: {
    width: Dimensions.get('window').width/2 * 1.33,
    height: 37,
    marginTop: 52,
    alignSelf: 'center',
    borderRadius: 4
  },
  faceicon: {
    marginLeft: 20,
    resizeMode: 'center',
  },
  facetext: {
    width: Dimensions.get('window').width/2 * 1.33 - 60,
    height: 37,
    marginLeft: 15,
    borderRadius: 4,
    fontSize: 16,
    color: 'white',
    paddingTop: 9
  },
  facebutton: {
    width: Dimensions.get('window').width/2 * 1.33,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#0756b2',
    borderRadius: 4
  },
  bottom: {
    padding:60,
    alignItems: 'center',
    width: null,
    height: 37,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  bottomtext: {
    width: null,
    height: 50,
    fontSize: 12,
    color: 'white',
    paddingTop: 9,
  },
  containerStyle: {
      flex: 1,
      flexDirection: 'row',
      padding: 10,
      alignItems: 'center'
  },
  labelStyle: {
      flex: 1,
      backgroundColor: 'rgba(0,0,0,0)',
      fontSize: 12,
      color: 'white',
      marginRight: 50
  },
  checkboxStyle: {
      width: 16,
      height: 16,
      borderWidth: 2,
      borderColor: 'white',
      borderRadius: 5,
      marginLeft: 50
  }
});
