import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  Navigator,
  TextInput,
  TouchableHighlight,
  ListView,
  View
} from 'react-native';

import {Scene, Router} from 'react-native-router-flux';

import NavItems from './components/NavItems'

import login from './screens/login';
import signup from './screens/signup';

export default class app extends Component{
  render() {
    return (
      <Router>
        <Scene initial key="login" titleStyle={{ color: '#9b249b' }} component={login} title="Iniciar Sesión" />
        <Scene key="signup" titleStyle={{ color: '#9b249b' }} component={signup} title="Registro" />
      </Router>
    );
  }
}
